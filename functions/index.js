const functions = require('firebase-functions');
const express = require('express');
const app = express();
const dialogflow = require('dialogflow');
const uuid = require('uuid')

app.post('/home',(req,res) =>{
   // runSample(res)
   //const user = JSON.stringify(request.body)
   //res.send('q: ' + req.body.name);
   runSample(req.body.question,res);

});

// app.get('/home2',(req,res) =>{
//     //res.send('esponse:');
//     res.send('id: ' + req.query.id);
// });

async function runSample(text,res) {
    // A unique identifier for the given session
    const sessionId = uuid.v4();
  
    // Create a new session
    const sessionClient = new dialogflow.SessionsClient();
    const sessionPath = sessionClient.sessionPath("analytics-679f9", sessionId);
  
    // The text query request.
    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          // The query to send to the dialogflow agent
          text: text,
          // The language used by the client (en-US)
          languageCode: 'en-US',
        },
      },
    };
  
    // Send request and log result
    const responses = await sessionClient.detectIntent(request);
    //console.log('Detected intent');
    const result = responses[0].queryResult;
    //console.log(`  Query: ${result.queryText}`);
    
    console.log(`  Response: ${result.fulfillmentText}`);
    var myResult = result.fulfillmentMessages[1].payload.fields.replies.listValue.values;

    console.log(JSON.stringify(myResult));
    
    if (myResult == null)
        res.json({"answer": ` ${result.fulfillmentText}` ,"replies": []});
    else
      res.json({"answer": ` ${result.fulfillmentText}` ,"replies": myResult});

    if (result.intent) {
      console.log(`  Intent: ${result.intent.displayName}`);
    } else {
      console.log(`  No intent matched.`);
    }
  }
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.app = functions.https.onRequest(app);
// exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {

//     response.send("hello");
//     console.log('result ' + request);
    
//   });
  
  
  
